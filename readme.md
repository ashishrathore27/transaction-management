# TRANSACTION-MANAGEMENT

Minimal [Spring Boot](http://projects.spring.io/spring-boot/) 

## Requirements

For building and running the application you need:

- [JDK 11](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Maven ](https://maven.apache.org)

## Running the application locally
  * please provide valid folder path as application environment variable 
    ````
    APPLICATION_FOLDER_PRODUCT_PATH : /home/administrator/Documents/assignment/product-details
    APPLICATION_FOLDER_TRANSACTION_PATH : /home/administrator/Documents/assignment/transaction-details
    ````
 
  * By Default scheduler will run every five minutes, For custom scheduling time
    ````
    APPLICATION_TRANSACTION_SCHEDULER_CORN:0 */5 * * * ?  
    ````
  
There are several ways to run a Spring Boot application on your local machine. One way is to execute the `main` method in the `com.uscrambl.transactionmanagement.TransactionManagementApplication` class from your IDE.

Alternatively you can use the [Spring Boot Maven plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html) like so:

```shell
mvn spring-boot:run
```

## API-DOC
http://localhost:8080/assignment/v2/api-docs

## Swagger UI
http://localhost:8080/assignment/swagger-ui.html

## Postman collection
src/test/resources/POSTMAN/transaction-management.postman_collection.json


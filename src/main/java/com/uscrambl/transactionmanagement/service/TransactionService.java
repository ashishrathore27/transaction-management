package com.uscrambl.transactionmanagement.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.uscrambl.transactionmanagement.model.Transaction;
import com.uscrambl.transactionmanagement.model.TransactionResponse;
import com.uscrambl.transactionmanagement.model.TransactionSummaryResponse;
import com.uscrambl.transactionmanagement.repository.TransactionRepository;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class TransactionService {

    private final TransactionRepository transactionRepository;
    private final ObjectMapper objectMapper;


    public TransactionService(TransactionRepository transactionRepository,
                              ObjectMapper objectMapper) {
        this.transactionRepository = transactionRepository;
        this.objectMapper = objectMapper;
    }

    public TransactionResponse getById(String transactionId) {
        Transaction transaction = transactionRepository.findById(transactionId).orElse(null);
        return objectMapper.convertValue(transaction, TransactionResponse.class);
    }

    public TransactionSummaryResponse getByProducts(String days) {
        List<Transaction> transactions = transactionRepository
                .findAllWithTransactionDatetime(new Date(), getNDaysBeforeDate.apply(days));
        Map<String, List<Transaction>> transactionByProducts =
                transactions.stream().collect(Collectors.groupingBy(transaction -> transaction.getProduct().getProductName()));

        return getTransactionSummaryResponse(transactionByProducts, TransactionSummaryResponse.SetProperty.PRODUCT);
    }

    public TransactionSummaryResponse getByCity(String days) {
        List<Transaction> transactions = transactionRepository
                .findAllWithTransactionDatetime(new Date(), getNDaysBeforeDate.apply(days));
        Map<String, List<Transaction>> transactionByProducts =
                transactions.stream().collect(Collectors.groupingBy(transaction -> transaction.getProduct().getProductManufacturingCity()));

        return getTransactionSummaryResponse(transactionByProducts, TransactionSummaryResponse.SetProperty.CITY);
    }


    private TransactionSummaryResponse getTransactionSummaryResponse(Map<String, List<Transaction>> transactionByProducts,
                                                                     TransactionSummaryResponse.SetProperty setProperty) {
        TransactionSummaryResponse transactionSummaryResponse = new TransactionSummaryResponse();
        transactionByProducts.forEach((k, v) -> {
            TransactionSummaryResponse.Summary summary = new TransactionSummaryResponse.Summary();
            if (setProperty.equals(TransactionSummaryResponse.SetProperty.CITY)) summary.setCityName(k);
            else summary.setProductName(k);
            updateAmount(v, summary);
            transactionSummaryResponse.addSummary(summary);
        });
        return transactionSummaryResponse;
    }

    private void updateAmount(List<Transaction> v, TransactionSummaryResponse.Summary summary) {
        Double totalAmount = v.stream().mapToDouble(transaction -> Double.parseDouble(transaction.getTransactionAmount())).sum();
        TransactionSummaryResponse.Amount amount = new TransactionSummaryResponse.Amount();
        amount.setTotalAmount(totalAmount);
        summary.setTotalAmount(amount);
    }

    private final Function<String, Date> getNDaysBeforeDate = days -> {
        Instant now = Instant.now();
        Instant before = now.minus(Duration.ofDays(Long.parseLong(days)));
        return Date.from(before);
    };
}

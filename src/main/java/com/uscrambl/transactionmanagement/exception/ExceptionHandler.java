package com.uscrambl.transactionmanagement.exception;

import com.uscrambl.transactionmanagement.scheduler.SharedScheduler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;

import java.io.IOException;

@ControllerAdvice
public class ExceptionHandler {
    Logger log = LoggerFactory.getLogger(SharedScheduler.class);

    @org.springframework.web.bind.annotation.ExceptionHandler(IOException.class)
    public ResponseEntity ioException(IOException exception){
        log.error(exception.getMessage(), exception);
        return new ResponseEntity("Invalid File format", HttpStatus.BAD_REQUEST);
    }
}

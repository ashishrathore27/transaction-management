package com.uscrambl.transactionmanagement.repository;

import com.uscrambl.transactionmanagement.model.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, String> {

    @Query("select t from Transaction t where (t.transactionDatetime <= :todayDate) AND (t.transactionDatetime >= :pastDate)")
    List<Transaction> findAllWithTransactionDatetime(@Param("todayDate") Date todayDate, @Param("pastDate") Date pastDate);
}

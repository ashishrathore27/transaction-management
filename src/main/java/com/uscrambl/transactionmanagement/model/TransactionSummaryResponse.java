package com.uscrambl.transactionmanagement.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class TransactionSummaryResponse {

    private List<Summary> summary = new ArrayList<>();

    public List<Summary> getSummary() {
        return summary;
    }

    public void setSummary(List<Summary> summary) {
        this.summary = summary;
    }
    public void addSummary(Summary summary){
        this.summary.add(summary);
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Summary{
        private String productName;
        private String cityName;
        private Amount totalAmount;

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public String getCityName() {
            return cityName;
        }

        public void setCityName(String cityName) {
            this.cityName = cityName;
        }

        public Amount getTotalAmount() {
            return totalAmount;
        }

        public void setTotalAmount(Amount totalAmount) {
            this.totalAmount = totalAmount;
        }
    }

    public static class Amount{
        private Double totalAmount;

        public Double getTotalAmount() {
            return totalAmount;
        }

        public void setTotalAmount(Double totalAmount) {
            this.totalAmount = totalAmount;
        }
    }

    public static enum SetProperty{
        CITY, PRODUCT
    }
}

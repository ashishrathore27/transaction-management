package com.uscrambl.transactionmanagement.util;

import net.sf.jsefa.Deserializer;
import net.sf.jsefa.common.lowlevel.filter.HeaderAndFooterFilter;
import net.sf.jsefa.csv.CsvIOFactory;
import net.sf.jsefa.csv.config.CsvConfiguration;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class FileUtil {

    public static <T> List<T> convertStreamIntoObjects(List<InputStream> file, Class<T> clazz, List<T> objects) {
        CsvConfiguration config = new CsvConfiguration();
        config.setFieldDelimiter(',');
        config.setLineFilter(new HeaderAndFooterFilter(1, false, true));
        Deserializer deserializer = CsvIOFactory.createFactory(config, clazz).createDeserializer();
        deserializer.open(new InputStreamReader(new SequenceInputStream(Collections.enumeration(file))));
        while (deserializer.hasNext()) {
            T Object = deserializer.next();
            objects.add(Object);
        }
        deserializer.close(true);
        return objects;
    }

    public static List<InputStream> getFilesInputStreams(String folderPath) throws IOException {
        List<InputStream> fileStreams = Files.walk(Paths.get(folderPath))
                .filter(Files::isRegularFile)
                .map(f -> {
                    try {
                        return new FileInputStream(f.toString());
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                }).collect(Collectors.toList());
        FileUtils.cleanDirectory(new File(folderPath));
        return fileStreams;
    }
}

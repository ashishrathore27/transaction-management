package com.uscrambl.transactionmanagement.scheduler;

import com.uscrambl.transactionmanagement.model.Product;
import com.uscrambl.transactionmanagement.model.Transaction;
import com.uscrambl.transactionmanagement.repository.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static com.uscrambl.transactionmanagement.util.FileUtil.convertStreamIntoObjects;
import static com.uscrambl.transactionmanagement.util.FileUtil.getFilesInputStreams;

@Component
public class SharedScheduler {
    Logger log = LoggerFactory.getLogger(SharedScheduler.class);

    @Value("${application.folder.transaction.path}")
    private String TRANSACTION_FOLDER_PATH;
    @Value("${application.folder.product.path}")
    private String PRODUCT_FOLDER_PATH;

    private final ProductRepository productRepository;

    public SharedScheduler(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Scheduled(cron = "${application.transaction.scheduler.corn}")
    public void readAndUpdateProductAndTransaction() throws IOException {
        List<InputStream> transactionStreams = getFilesInputStreams(TRANSACTION_FOLDER_PATH);
        List<InputStream> productStreams = getFilesInputStreams(PRODUCT_FOLDER_PATH);
        List<Transaction> transactions = convertStreamIntoObjects(transactionStreams, Transaction.class, new ArrayList<>());
        List<Product> products = convertStreamIntoObjects(productStreams, Product.class, new ArrayList<>());
        transactions.forEach(transaction -> {
            products.stream()
                    .filter(p -> p.getProductId().replaceAll("\\s+", "").equals(transaction.getProductId().replaceAll("\\s+", "")))
                    .findFirst()
                    .ifPresent(product1 -> product1.addTransaction(transaction));
        });
        productRepository.saveAll(products);
        log.info(products.size() + ":Products" + "," + transactions.size() + ":Transactions" +" "+"updated successfully");
    }


}

package com.uscrambl.transactionmanagement.resource;

import com.uscrambl.transactionmanagement.model.TransactionResponse;
import com.uscrambl.transactionmanagement.model.TransactionSummaryResponse;
import com.uscrambl.transactionmanagement.service.TransactionService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
public class TransactionResource {

    private final TransactionService transactionService;

    public TransactionResource(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @GetMapping("transaction/{transaction_id}")
    public TransactionResponse getById(@PathVariable("transaction_id") String transactionId) throws IOException {
        return transactionService.getById(transactionId);
    }

    @GetMapping("transactionSummaryByProducts/{last_n_days}")
    public TransactionSummaryResponse getByProducts(@PathVariable("last_n_days") String days) throws IOException {
        return transactionService.getByProducts(days);
    }

    @GetMapping("transactionSummaryByManufacturingCity/{last_n_days}")
    public TransactionSummaryResponse getByCity(@PathVariable("last_n_days") String days) throws IOException {
        return transactionService.getByCity(days);
    }
}
